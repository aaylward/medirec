import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='medirec',
    version='0.0.1',
    author='Anthony Aylward',
    author_email='anthony.aylward@gmail.com',
    description='Medication record',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/anthony-aylward/medirec',
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    install_requires = [
        'flask',
        'flask-login',
        'flask-mail',
        'flask-migrate',
        'flask-misaka',
        'flask-wtf',
        'pandas',
        'pyjwt',
        'misaka',
    ]
)
