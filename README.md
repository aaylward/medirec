# medirec
medication record

## On the production server
```
python3 setup.py bdist_wheel
source venv/bin/activate
pip3 install dist/medirec-[latest]-py3-none-any.whl
python3 config/__init__.py --production venv/var/medirec-instance/
cd venv/lib/python3.6/site-packages/
export FLASK_APP=medirec
flask db upgrade --directory ../../../../migrations
uwsgi --socket 127.0.0.1:[port] --wsgi-file medirec_uwsgi --callable app --processes 2 --threads 2 --env MAIL_PASSWORD=[mail server password]
```
See also a [neat video on uWSGI](https://www.youtube.com/watch?v=2IeSPvkQEtw)

Managing the backup drive:
```
udisksctl mount -b /dev/sda1
udisksctl unmount -b /dev/sda1
udisksctl power-off -b /dev/sda1
```
