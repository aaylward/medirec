#===============================================================================
# strategy.py
#===============================================================================

"""Strategy blueprint

Attributes
----------
bp : Blueprint
    blueprint object, see the flask tutorial/documentation:

    http://flask.pocoo.org/docs/1.0/tutorial/views/

    http://flask.pocoo.org/docs/1.0/blueprints/
"""




# Imports ======================================================================

import os.path
import pandas as pd

from datetime import datetime
from flask import (
    Blueprint, flash, render_template, current_app, url_for, redirect, request
)
from medirec.models import get_db, MedRecord

from medirec.forms import MedForm



# Blueprint assignment =========================================================

bp = Blueprint('record', __name__)




# Constants ====================================================================

RECORD = """
<center>

| Medication | Dose | Time |
| --- | --- | --- |
{}

</center>
"""


# Functions ====================================================================

@bp.route('/', methods=('GET', 'POST'))
def index():
    f"""Render the record index"""
    db = get_db()
    records = MedRecord.query.order_by(MedRecord.timestamp.desc())
    time_format = '%a %b %d %I:%M %p'
    time_format_sans_date = '%I:%M %p'
    form = MedForm()
    if form.validate_on_submit():
        if form.undo.data:
            newest_record = MedRecord.query.order_by(MedRecord.timestamp.desc()).first()
            med, dose, unit = newest_record.med, newest_record.dose, newest_record.unit
            db.session.delete(newest_record)
            db.session.commit()
            flash(f'You didnt take {med} {dose} {unit} at {newest_record.timestamp.strftime(time_format_sans_date)}', 'danger')
            return redirect(url_for('record.index'))
        elif form.download.data:
            return redirect(url_for('protected.protected',filename='medirec.csv'))
        elif form.apap.data:
            med, dose, unit = 'APAP', 500, 'mg'
        elif form.benzonatate.data:
            med, dose, unit = 'Benzonatate', 200, 'mg'
        elif form.dex.data:
            med, dose, unit = 'Dexamethasone', 4, 'mg'
        elif form.diclo.data:
            med, dose, unit = 'Diclofenac', 100, 'mg'
        elif form.docu.data:
            med, dose, unit = 'Docusate', 100, 'mg'
        elif form.gaba.data:
            med, dose, unit = 'Gabapentin', 600, 'mg'
        elif form.norco.data:
            med, dose, unit = 'Norco', 1, 'x 5-325 mg'
        elif form.onda.data:
            med, dose, unit = 'Ondasetron', 8, 'mg'
        elif form.oxy.data:
            med, dose, unit = 'Oxycodone', 5, 'mg'
        elif form.tramadol.data:
            med, dose, unit = 'Tramadol', 50, 'mg'
        record = MedRecord(med=med, dose=dose, unit=unit)
        db.session.add(record)
        row_count = MedRecord.query.count()
        if row_count > 8:
            oldest_record = MedRecord.query.order_by(MedRecord.timestamp.asc()).first()
            db.session.delete(oldest_record)
        db.session.commit()
        pd.DataFrame(
            [(r.med, r.dose, r.unit) for r in records],
            columns=('Medication', 'Dose', 'Time')
        ).to_csv(
            os.path.join(current_app.config['PROTECTED_DIR'], 'medirec.csv'),
            index=False
        )
        flash(f'You took {med} {dose} {unit} at {datetime.now().strftime(time_format_sans_date)}', 'primary')
        return redirect(url_for('record.index'))
    
    return render_template(
        'record/index.html',
        form = form,
        record=RECORD.format(
            '\n'.join(
                f'| {r.med} | {r.dose} {r.unit} | {r.timestamp.strftime(time_format)} |' for r in records
            )
        )
    )
