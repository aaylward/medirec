#===============================================================================
# models.py
#===============================================================================

"""Databse models

Attributes
----------
db : SQLAlchemy
    The database object (see Flask-SQLAlchemy:
    http://flask-sqlalchemy.pocoo.org/2.3/ )
migrate : Migrate
    The Migrate object (see Flask-Migrate:
    https://flask-migrate.readthedocs.io/en/latest/ )
"""




# Imports ======================================================================

from time import time
from datetime import datetime
from flask import g, current_app
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy




# Database initialization ======================================================

db = SQLAlchemy()
migrate = Migrate()




# Classes ======================================================================

# Models -----------------------------------------------------------------------

class MedRecord(db.Model):
    """A comment

    Attributes
    ----------
    id : int
    med : str
    timestamp : datetime
    """

    id = db.Column(db.Integer, primary_key=True)
    med = db.Column(db.String(256))
    dose = db.Column(db.Integer)
    unit = db.Column(db.String(256))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        """String representation of the record
        
        Returns
        -------
        str
            String representation of the record
        """
        
        return f'<MedRecord {self.med} {self.timestamp}>'




# Functions ====================================================================

def get_db():
    """Retrieve the database from the application context
    
    Returns
    -------
    SQLAlchemy
        The database object
    """

    if 'db' not in g:
        g.db = db
    return g.db
