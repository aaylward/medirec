#===============================================================================
# __init__.py
#===============================================================================

"""Initialization

This file contains the application factory for the medirec app.
"""




# Imports ======================================================================

import os

from flask import Flask, render_template
from flask_login import login_required




# Functions ====================================================================

def create_app(test_config=None):
    """The application factory function

    This function creates and configures the Flask application object. For
    more on application factories, see the Flask documentation/tutorial:

    http://flask.pocoo.org/docs/1.0/tutorial/factory/

    http://flask.pocoo.org/docs/1.0/patterns/appfactories/

    Parameters
    ----------
    test_config : dict
        A dictionary containing configuration parameters for use during unit
        testing. If this parameter is `None`, the configuration will be loaded
        from `config.py` in the instance folder.

    Returns
    -------
    Flask
        A flask app
    """

    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    from medirec.models import db, migrate
    # from medirec.login import login
    # from medirec.email import mail
    from medirec.errors import forbidden
    from medirec.misaka import md
    for ext in db, md:
        ext.init_app(app)
    migrate.init_app(app, db)
    # login.login_view = 'auth.login'

    for error, handler in ((403, forbidden),):
        app.register_error_handler(403, forbidden)
    
    from medirec import record, protected
    for bp in record.bp, protected.bp:
        app.register_blueprint(bp)
    
    app.add_url_rule('/', endpoint='record.index')
    
    return app
