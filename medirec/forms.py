#===============================================================================
# forms.py
#===============================================================================

"""Forms (subclasses of FlaskForm, see Flask-WTF:
http://flask-wtf.readthedocs.io/en/stable/ )
"""




# Imports  =====================================================================

from datetime import datetime

from flask import request, current_app
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import (
    SubmitField
)
from wtforms.validators import (
    ValidationError, DataRequired, Email, EqualTo, Length
)




# Classes ======================================================================

# Forms ------------------------------------------------------------------------

class MedForm(FlaskForm):
    """A form for strategy queries

    Attributes
    ----------
    submit : SubmitField
    """
    
    undo = SubmitField('Undo')
    download = SubmitField('Download CSV')
    apap = SubmitField('Take APAP')
    benzonatate = SubmitField('Take Benzonatate')
    dex = SubmitField('Take Dexamethasone')
    diclo = SubmitField('Take Diclofenac')
    docu = SubmitField('Take Docusate')
    gaba = SubmitField('Take Gabapentin')
    norco = SubmitField('Take Norco')
    onda = SubmitField('Take Ondasetron')
    oxy = SubmitField('Take Oxycodone')
    tramadol = SubmitField('Take Tramadol')
